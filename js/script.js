//cria o canvas
var cnv = document.querySelector("canvas");
var ctx = document.querySelector("canvas").getContext("2d");

ctx.canvas.height = 900;
ctx.canvas.width = 1880;

//musica e sons
musicaFundo = new Audio('ost.mp3')
somPulo = new Audio('jump.wav')

//Arrays
var sprites = [];
var blocks = [];

//Objetos instanciados com os seguintes parâmetros: x, y, Largura, Altura e cor
var player1 = new Sprite(1800, 600, 20, 40, "#ff7500");
player1.jumping=false;
player1.crouch=false;
player1.y_velocity= 0;
player1.x_velocity= 0;
sprites.push(player1);

var paredespawn = new Sprite(1860, 400, 60, 500, "#b1651c");
sprites.push(paredespawn);
blocks.push(paredespawn);

var piso1 = new Sprite(600, 860, 200, 40, "#7a3918");
sprites.push(piso1);
blocks.push(piso1);

var pisospawn = new Sprite(1680, 860, 200, 40, "#7a3918");
sprites.push(pisospawn);
blocks.push(pisospawn);

var piso2 = new Sprite(1050, 860, 360, 40, "#7a3918");
sprites.push(piso2);
blocks.push(piso2);

var piso4 = new Sprite(0, 860, 200, 40, "#7a3918");
sprites.push(piso4);
blocks.push(piso4);

var block11 = new Sprite(380, 860, 60, 40, "#7a3918");
sprites.push(block11);
blocks.push(block11);

var block2 = new Sprite(500, 440, 60, 20, "#b1651c");
sprites.push(block2);
blocks.push(block2);

var block3 = new Sprite(100, 640, 60, 20, "#b1651c");
sprites.push(block3);
blocks.push(block3);

var block4 = new Sprite(300, 520, 60, 20, "#b1651c");
sprites.push(block4);
blocks.push(block4);

var block5 = new Sprite(700, 520, 60, 20, "#b1651c");
sprites.push(block5);
blocks.push(block5);

var block6 = new Sprite(900, 440, 60, 20, "#b1651c");
sprites.push(block6);
blocks.push(block6);

var block7 = new Sprite(1100, 340, 60, 500, "#b1651c");
sprites.push(block7);
blocks.push(block7);

var block8 = new Sprite(1300, 240, 60, 600, "#b1651c");
sprites.push(block8);
blocks.push(block8);

var block9 = new Sprite(1500, 200, 60, 200, "#b1651c");
sprites.push(block9);
blocks.push(block9);

var block12 = new Sprite(1500, 360, 400, 40, "#7a3918");
sprites.push(block12);
blocks.push(block12);

//controle do player1
controle = {

	esquerda:false,
	direita:false,
	cima:false,
	baixo:false,
	keyListener:function(event) {

	var key_state = (event.type == "keydown")?true:false;

  	switch(event.keyCode) {

			case 65:// esquerda botao
  			controle.esquerda = key_state;
  			break;
			case 87:// cima botao
  			controle.cima = key_state;
  			break;
			case 68:// direita botao
  			controle.direita = key_state;
  			break;
			case 83:// baixo botao
  			controle.baixo  = key_state;
  			break;

  	}

	}

};

//funções
function loop(){
	window.requestAnimationFrame(loop);
	update();
	render();
}

	//Atualizações
	function update(){
		musicaFundo.play();
		musicaFundo.volume= 0.3;
		player1.y_velocity += 2.5;// gravidade
		player1.x += player1.x_velocity;
		player1.y += player1.y_velocity;
		player1.x_velocity *= 0.8;// fricção
		player1.y_velocity *= 0.8;// fricção

		//condicao levantar
		if (controle.cima && player1.crouch == true && player1.jumping == false) {
			player1.y-=20;
			player1.jumping=false;
	    	player1.height=40;
			player1.crouch=false;
		}
		//condicao para pular
		if (controle.cima && player1.jumping == false && player1.crouch == false) {
			somPulo.play();
			somPulo.volume= 0.5;
			player1.y_velocity -= 40;
    		player1.jumping = true;
      		player1.crouch=false;
		}
		//condicao andar para esquerda
		if (controle.esquerda) {
    		player1.x_velocity -= 2;
		}
		//condicao andar para direita
		if (controle.direita) {
    		player1.x_velocity += 2;
		}
		//condicao abaixar
		if (controle.baixo && player1.jumping==false  && player1.crouch == false) {
			player1.height=20;
			player1.crouch=true;
			player1.jumping=false;
		}
		//win
		if (player1.x>=1800 && player1.y<360 && player1.y>260){
			win();
		}
			//game over
        if (player1.y>=870){
            endgame();
        }

		//Limites da tela
		player1.x = Math.max(0, Math.min(cnv.width - player1.width, player1.x));
		player1.y = Math.max(0, Math.min(cnv.height - player1.height, player1.y));

		//Colisões
		for(var i in blocks){
			var blk = blocks[i];
			if(blk.visible){
				blockRect(player1,blk);
			}
		}
	}

	//Renderização ou desenho na tela
	function render(){
		ctx.clearRect(0,0,cnv.width,cnv.height);

        var door = new Image();
        door.src = 'bola.png';
        ctx.drawImage(door, 1800, 265, 70, 100);

		for(var i in sprites){
			var spr = sprites[i];
			if(spr.visible){
				ctx.fillStyle = spr.color;
				ctx.fillRect(spr.x, spr.y, spr.width, spr.height);
			}
		}
	}
		//detecção de input
    window.addEventListener("keydown", controle.keyListener);
    window.addEventListener("keyup", controle.keyListener);

	loop();
