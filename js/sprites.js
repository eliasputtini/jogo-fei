//Função pra criar Blocos
var Sprite = function(x, y, width, height, color, src){
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.color = color;
	this.visible = true;
	this.src = src;
}

//Retorna a metade da largura
Sprite.prototype.halfWidth = function(){
	return this.width/2;
}
//Retorna a metade da altura
Sprite.prototype.halfHeight = function(){
	return this.height/2;
}
//Retorna a posição do centro do objeto no eixo X
Sprite.prototype.centerX = function(){
	return this.x + this.halfWidth();
}
//Retorna a posição do centro do objeto no eixo Y
Sprite.prototype.centerY = function(){
	return this.y + this.halfHeight();
}

//funcao fim do game
function endgame(){
	alert("GAME OVER! clique aqui para reiniciar");
  	document.location.reload();
	}

// funcao vitoria
function win(){
    alert("YOU WIN! clique aqui para reiniciar");
    document.location.reload();
}
