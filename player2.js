var player2 = new Sprite(0, 200, 20, 40, "red");
player2.jumping=false;
player2.y_velocity= 0;
player2.x_velocity= 0;
sprites.push(player2);

//controle2 do player2
controle2 = {

    esquerda:false,
    direita:false,
    cima:false,
    baixo:false,
    keyListener:function(event) {
        var key_state = (event.type == "keydown")?true:false;
        switch(event.keyCode) {
            case 74:// esquerda botao J
                controle2.esquerda = key_state;
                break;
            case 73:// cima botao I
                controle2.cima = key_state;
                break;
            case 76:// direita botaov L
                controle2.direita = key_state;
                break;
            case 75:// baixo botao K
                controle2.baixo  = key_state;
                break;
        }
    }
};

update2();

//Atualizações
function update2(){
      player2.y_velocity += 1.5;// gravidade
      player2.x += player2.x_velocity;
      player2.y += player2.y_velocity;
      player2.x_velocity *= 0.9;// fricção
      player2.y_velocity *= 0.9;// fricção

      if (controle2.cima && player2.jumping == false) {
          player2.y -= 20;
          player2.y_velocity -= 30;
          player2.jumping = true;
          player2.height=40;

      }

      if (controle2.esquerda) {
          player2.x_velocity -= 0.9;
      }

      if (controle2.direita) {
          player2.x_velocity += 0.9;
      }

      if (controle2.baixo) {
          player2.height=20;
      }

  //Limites da tela
  player2.x = Math.max(0, Math.min(cnv.width - player2.width, player2.x));
  player2.y = Math.max(0, Math.min(cnv.height - player2.height, player2.y));

  //Colisões
  //for(var i in blocks){
    //var blk = blocks[i];
    //if(blk.visible){
      //blockRect(player2,blk);
    //}
  //}

  window.requestAnimationFrame(update2);
}

window.addEventListener("keydown", controle2.keyListener);
window.addEventListener("keyup", controle2.keyListener);
